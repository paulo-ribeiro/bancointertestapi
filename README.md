# BANCO-INTER-TEST API
O projeto utiliza Spring Boot na versão 2.1.0 com as dependencias DevTools, Web, JPA e H2 databse.

Na pasta postman-collection se encontra a collection do postman usada para testar a API.

##Rodando o projeto
Para buildar e rodar o projeto basta executar o comando "mvn spring-boot:run" no diretório raiz.

Foi criado também o arquivo compileRun.sh para facilitar ainda mais o processo. Em um ambiente Linux basta executar o comando "./compileRun.sh" para compilar e executar o projeto.

##Informações sobre o candidato
Desenvolvedor da Squadra Tecnologia que deseja trabalhar alocado no Banco Inter.
Contato: paulo.alves@squadra.com.br
Equipe: Núcleo Java
package com.banco.inter.test.api.BancoInterTestApi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
@RestController
public class CustomResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
	
	@ExceptionHandler(InvalidIdException.class)
	public final ResponseEntity<Object> invalidIdException(InvalidIdException ex, WebRequest request) {
		InvalidIdExceptionResponse exceptionResponse = new InvalidIdExceptionResponse(ex.getMessage());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InvalidJobParentException.class)
	public final ResponseEntity<Object> invalidJobParentException(InvalidJobParentException ex, WebRequest request) {
		InvalidJobParentException exceptionResponse = new InvalidJobParentException(ex.getMessage());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(InvalidJobNameException.class)
	public final ResponseEntity<Object> invalidJobNameException(InvalidJobNameException ex, WebRequest request) {
		InvalidJobNameException exceptionResponse = new InvalidJobNameException(ex.getMessage());
		return new ResponseEntity<Object>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}
}

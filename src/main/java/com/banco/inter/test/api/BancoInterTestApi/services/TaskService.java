package com.banco.inter.test.api.BancoInterTestApi.services;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.inter.test.api.BancoInterTestApi.domain.entities.Task;
import com.banco.inter.test.api.BancoInterTestApi.exceptions.InvalidIdException;
import com.banco.inter.test.api.BancoInterTestApi.repositories.TaskRepository;

@Service
public class TaskService {
	
	@Autowired
	private TaskRepository taskRepository;
	
	public Task findTaskById(Long taskId) {
		Optional<Task> task = taskRepository.findById(taskId);
		
		if(!task.isPresent())
			throw new InvalidIdException("Task ID " + taskId + " does not exist.");
		
		return task.get();
	}
	
	public Task saveTask(Task task) {
		if(taskExists(task.getId()))
			throw new InvalidIdException("Task ID " + task.getId() + " already exists.");
		
		return taskRepository.save(task);
	}
	
	private boolean taskExists(Long taskId) {
		try {
			findTaskById(taskId);
			return true;
		} catch(InvalidIdException e) {
			return false;
		}
	}
	
	public Task updateTask(Task task) {
		if(findTaskById(task.getId()) != null)
			return taskRepository.save(task);
		
		return null;
	}
	
	public Iterable<Task> findAllTasks(Date createdAt) {
		if(createdAt != null)
			return taskRepository.findByCreatedAt(createdAt);
		else
			return taskRepository.findAll();
	}
	
	public void deleteTask(Long taskId) {
		Task task = findTaskById(taskId);		
		taskRepository.delete(task);
	}
}

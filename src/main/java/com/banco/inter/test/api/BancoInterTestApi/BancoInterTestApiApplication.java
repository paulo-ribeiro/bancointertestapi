package com.banco.inter.test.api.BancoInterTestApi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoInterTestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoInterTestApiApplication.class, args);
	}
}

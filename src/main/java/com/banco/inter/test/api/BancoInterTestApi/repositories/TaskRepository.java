package com.banco.inter.test.api.BancoInterTestApi.repositories;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.banco.inter.test.api.BancoInterTestApi.domain.entities.Task;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
	
	@Override
	Iterable<Task> findAll();
	
	@Query("SELECT t FROM Task t WHERE t.createdAt = :createdAt")
	Iterable<Task> findByCreatedAt(@Param("createdAt") Date createdAt);
	
	@Override
	Optional<Task> findById(Long id);
}

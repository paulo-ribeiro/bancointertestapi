package com.banco.inter.test.api.BancoInterTestApi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidJobParentException extends RuntimeException {
	
	public InvalidJobParentException(String message) {
		super(message);
	}
}

package com.banco.inter.test.api.BancoInterTestApi.web;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banco.inter.test.api.BancoInterTestApi.domain.dtos.GenericResponse;
import com.banco.inter.test.api.BancoInterTestApi.domain.entities.Job;
import com.banco.inter.test.api.BancoInterTestApi.services.JobService;
import com.banco.inter.test.api.BancoInterTestApi.services.MapValidationErrorService;

@RestController
@RequestMapping("/jobs")
public class JobController {
	
	@Autowired
	private JobService jobService;
	
	@Autowired
	private MapValidationErrorService mapValidationErrorService;
	
	@PostMapping("")
	public ResponseEntity<?> createNewJob(@Valid @RequestBody Job job, BindingResult result) {
		ResponseEntity<?> errorResponse = mapValidationErrorService.objectValidation(result);
		if(errorResponse != null) return errorResponse;
		
		Job jobCreated = jobService.saveJob(job);
		return new ResponseEntity<Job>(jobCreated, HttpStatus.CREATED);
	}
	
	@GetMapping("")
	public Iterable<Job> getAllJobs(@RequestParam(value="sortByWeight") Boolean sortByWeight) {
		return jobService.findAllJobs(sortByWeight);
	}
	
	@GetMapping("/{id}")
	public Job getJobById(@PathVariable Long id) {
		return jobService.findJobById(id);	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteJob(@PathVariable Long id) {
		jobService.deleteJob(id);
		return new ResponseEntity<GenericResponse>(new GenericResponse("Job ID " + id + " deleted."), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateTask(@PathVariable Long id, @Valid @RequestBody Job job, BindingResult result) {
		ResponseEntity<?> errorResponse = mapValidationErrorService.objectValidation(result);
		if(errorResponse != null) return errorResponse;
		
		Job jobUpdated = jobService.updateJob(job);
		return new ResponseEntity<Job>(jobUpdated, HttpStatus.OK);
	}
}

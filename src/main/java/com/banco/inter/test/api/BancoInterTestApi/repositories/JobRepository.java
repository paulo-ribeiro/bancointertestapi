package com.banco.inter.test.api.BancoInterTestApi.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.banco.inter.test.api.BancoInterTestApi.domain.entities.Job;

@Repository
public interface JobRepository extends CrudRepository<Job, Long>{
	
	@Override
	Iterable<Job> findAll();
	
	@Query("SELECT j FROM Job j LEFT JOIN j.tasks AS t GROUP BY j ORDER BY SUM(t.weight)")
	Iterable<Job> findAllOrderByTaskWeightSum();
	
	@Override
	Optional<Job> findById(Long id);
	
	@Query("SELECT j FROM Job j WHERE j.name = :name")
	Optional<Job> findByName(@Param("name") String name);
}

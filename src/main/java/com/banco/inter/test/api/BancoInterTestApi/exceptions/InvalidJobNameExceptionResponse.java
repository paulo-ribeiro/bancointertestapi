package com.banco.inter.test.api.BancoInterTestApi.exceptions;

public class InvalidJobNameExceptionResponse {
	
	private String name;
	
	public InvalidJobNameExceptionResponse(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

package com.banco.inter.test.api.BancoInterTestApi.domain.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Task {
	
	@NotNull(message = "Task id is required.")
	@Id
	private Long id;
	
	@NotBlank(message = "Task name is required.")
	private String name;
	
	@NotNull(message = "Task weight is required.")
	private Integer weight;
	
	private Boolean completed;
	
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date createdAt;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getWeight() {
		return weight;
	}

	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	public Boolean getCompleted() {
		return completed;
	}

	public void setCompleted(Boolean completed) {
		this.completed = completed;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	
	@PrePersist
	private void onCreate() {
		if(completed == null) completed = Boolean.FALSE;
		createdAt = new Date();
	}
}

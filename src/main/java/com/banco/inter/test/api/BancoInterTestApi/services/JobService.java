package com.banco.inter.test.api.BancoInterTestApi.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.banco.inter.test.api.BancoInterTestApi.domain.entities.Job;
import com.banco.inter.test.api.BancoInterTestApi.exceptions.InvalidIdException;
import com.banco.inter.test.api.BancoInterTestApi.exceptions.InvalidJobNameException;
import com.banco.inter.test.api.BancoInterTestApi.exceptions.InvalidJobParentException;
import com.banco.inter.test.api.BancoInterTestApi.repositories.JobRepository;

@Service
public class JobService {
	
	@Autowired
	private JobRepository jobRepository;
	
	public Job findJobById(Long jobId) {
		Optional<Job> job = jobRepository.findById(jobId);
		
		if(!job.isPresent())
			throw new InvalidIdException("Job ID " + jobId + " does not exist.");
		
		return job.get();
	}
	
	public Job findJobByName(String name) {
		Optional<Job> job = jobRepository.findByName(name);
		
		if(!job.isPresent())
			throw new InvalidJobNameException("Job name '" + name + "' does not exist.");
		
		return job.get();
	}
	
	public Job saveJob(Job job) {
		if(jobExists(job.getId()))
			throw new InvalidIdException("Job ID " + job.getId() + " already exists.");
		
		if(jobNameExists(job.getName()))
			throw new InvalidJobNameException("Job name '" + job.getName() + "' already.");
		
		if(job.getParent() != null) {
			if(!jobExists(job.getParent().getId()))
				throw new InvalidJobParentException("Job parent ID " + job.getParent().getId() + " does not exist.");
			
			if(job.getId().equals(job.getParent().getId()))
				throw new InvalidJobParentException("Job ID " + job.getParent().getId() + " cant be his own parent.");
		}
		
		return jobRepository.save(job);
	}
	
	private boolean jobExists(Long jobId) {
		try {
			findJobById(jobId);
			return true;
		} catch(InvalidIdException e) {
			return false;
		}
	}
	
	private boolean jobNameExists(String name) {
		try {
			findJobByName(name);
			return true;
		} catch(InvalidJobNameException e) {
			return false;
		}
	}
	
	public Job updateJob(Job job) {
		if(findJobById(job.getId()) != null)
			return jobRepository.save(job);
		
		return null;
	}
	
	public Iterable<Job> findAllJobs(Boolean sortByWeight) {
		if(sortByWeight != null && sortByWeight)
			return jobRepository.findAllOrderByTaskWeightSum();
		else
			return jobRepository.findAll();
	}
	
	public void deleteJob(Long jobId) {
		Job job = findJobById(jobId);		
		jobRepository.delete(job);
	}
}

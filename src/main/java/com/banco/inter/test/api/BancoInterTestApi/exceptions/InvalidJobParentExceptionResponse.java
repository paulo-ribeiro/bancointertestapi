package com.banco.inter.test.api.BancoInterTestApi.exceptions;

public class InvalidJobParentExceptionResponse {
	
	private String parent;
	
	public InvalidJobParentExceptionResponse(String parent) {
		this.parent = parent;
	}
	
	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}
}

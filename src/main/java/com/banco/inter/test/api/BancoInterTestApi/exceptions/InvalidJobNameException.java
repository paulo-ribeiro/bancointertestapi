package com.banco.inter.test.api.BancoInterTestApi.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class InvalidJobNameException extends RuntimeException {
	
	public InvalidJobNameException(String message) {
		super(message);
	}
}

package com.banco.inter.test.api.BancoInterTestApi.web;

import java.util.Date;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banco.inter.test.api.BancoInterTestApi.domain.dtos.GenericResponse;
import com.banco.inter.test.api.BancoInterTestApi.domain.entities.Task;
import com.banco.inter.test.api.BancoInterTestApi.services.MapValidationErrorService;
import com.banco.inter.test.api.BancoInterTestApi.services.TaskService;

@RestController
@RequestMapping("/tasks")
public class TaskController {
	
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private MapValidationErrorService mapValidationErrorService;
	
	@PostMapping("")
	public ResponseEntity<?> createNewTask(@Valid @RequestBody Task task, BindingResult result) {
		ResponseEntity<?> errorResponse = mapValidationErrorService.objectValidation(result);
		if(errorResponse != null) return errorResponse;
		
		Task taskCreated = taskService.saveTask(task);
		return new ResponseEntity<Task>(taskCreated, HttpStatus.CREATED);
	}
	
	@GetMapping("")
	public Iterable<Task> getAllTasks(@RequestParam(value="createdAt") @DateTimeFormat(pattern="yyyy-MM-dd") Date createdAt) {
		return taskService.findAllTasks(createdAt);
	}
	
	@GetMapping("/{id}")
	public Task getTaskById(@PathVariable Long id) {
		return taskService.findTaskById(id);	
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<?> deleteTask(@PathVariable Long id) {
		taskService.deleteTask(id);
		return new ResponseEntity<GenericResponse>(new GenericResponse("Task ID " + id + " deleted."), HttpStatus.OK);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<?> updateTask(@PathVariable Long id, @Valid @RequestBody Task task, BindingResult result) {
		ResponseEntity<?> errorResponse = mapValidationErrorService.objectValidation(result);
		if(errorResponse != null) return errorResponse;
		
		Task taskUpdated = taskService.updateTask(task);
		return new ResponseEntity<Task>(taskUpdated, HttpStatus.OK);
	}
}

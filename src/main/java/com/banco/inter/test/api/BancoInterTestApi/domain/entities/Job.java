package com.banco.inter.test.api.BancoInterTestApi.domain.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Job {
	
	@NotNull(message = "Job id is required.")
	@Id
	private Long id;
	
	@NotBlank(message = "Job name is required.")
	@Column(unique = true)
	private String name;
	
	@NotNull(message = "Job active is required.")
	private Boolean active;
	
	@OneToMany
	private List<Task> tasks;
	
	@ManyToOne
	private Job parent;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public Boolean getActive() {
		return active;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	public List<Task> getTasks() {
		return tasks;
	}
	
	public void setTasks(List<Task> tasks) {
		this.tasks = tasks;
	}
	
	public Job getParent() {
		return parent;
	}
	
	public void setParent(Job parent) {
		this.parent = parent;
	}
}

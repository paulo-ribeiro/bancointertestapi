package com.banco.inter.test.api.BancoInterTestApi.exceptions;

public class InvalidIdExceptionResponse {
	
	private String id;

	public InvalidIdExceptionResponse(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
}
